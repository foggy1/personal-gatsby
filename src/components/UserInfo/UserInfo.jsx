import React, { Component } from "react";
import { Follow } from "react-twitter-widgets";

class UserInfo extends Component {
  render() {
    const { userTwitter } = this.props.config;
    const { expanded } = this.props;
    return (
      <a
        href={`https://mastodon.jumanji.co/@foggy`}
        // username={userTwitter}
        // options={{ count: expanded ? true : "none" }}
        style={{backgroundColor: '#2791DA'}}
        target='_blank'
        rel='noopener'
        className='button'
      > 
        <span class="icon">
          <i style={{color: 'white'}} className="fab fa-mastodon"></i>
        </span>
        <span style={{color: 'white'}}>Follow on Mastodon</span>
      </a>
    );
  }
}

export default UserInfo;
