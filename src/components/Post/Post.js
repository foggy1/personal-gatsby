import React from "react";
import Helmet from "react-helmet";
import UserInfo from "../UserInfo/UserInfo";
import UserLinks from '../UserLinks/UserLinks'
import Disqus from "../Disqus/Disqus";
import PostTags from "../PostTags/PostTags";
import SocialLinks from "../SocialLinks/SocialLinks";
import SEO from "../SEO/SEO";
import Link from 'gatsby-link'
import config from '../../../data/SiteConfig.js'

const Post = (props) => {
  const slug = props.slug || props.indexPostNode.fields.slug
  const postNode = props.indexPostNode || props.postNode
  const post = postNode.frontmatter
  return(<div>
        {props.data ? (
          <Helmet>
            <title>{`${post.title} | ${config.siteTitle}`}</title>
          </Helmet>) : <div />
        }
        <SEO postPath={slug} postNode={postNode} postSEO />
        <div className={props.data ? 'container' : ''}>
        <div className='section'>
        {props.data? <h1 className='title'>{post.title}</h1> : (
          <h1 className='title'>
            <Link to={slug}>{post.title}</Link>
          </h1>
        )}
            <div className='content' dangerouslySetInnerHTML={{ __html: postNode.html }} />
            {props.data ? <UserInfo postPath={slug} className='section' config={config} /> : <div />}
          </div>
          <div className="section post-meta">
            <PostTags tags={post.tags} />
            {props.data ? <SocialLinks postNode={postNode} /> : <div />}
          </div>
        {props.data ? <span /> : <hr />}
      </div>
</div>
  )
}

export default Post
