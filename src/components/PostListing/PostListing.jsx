import React from "react";
import Link from "gatsby-link";
import moment from 'moment'
import Post from '../../components/Post/Post'
class PostListing extends React.Component {
  getPostList() {
    const postList = [];
    this.props.postEdges.forEach(postEdge => {
      postList.push({
        path: postEdge.node.fields.slug,
        tags: postEdge.node.frontmatter.tags,
        cover: postEdge.node.frontmatter.cover,
        title: postEdge.node.frontmatter.title,
        date: postEdge.node.frontmatter.date,
        excerpt: postEdge.node.excerpt,
        timeToRead: postEdge.node.timeToRead
      });
    });
    return postList;
  }
  render() {
    const postList = this.getPostList();
    return (
      <div className='section'>
        <div className='container'>
          <div className='columns'>
          <ul className='column is-three-quarters'>
            {/* Your post list here. */
              this.props.postEdges.map(post => {
                return <Post indexPostNode={post.node} pathContext={post.node.fields} />
              })}
          </ul>
          <div style={{paddingTop: '3.75rem'}} className='column is-one-quarter'>
            <h1>Contact:</h1>
            <br />
            <ul>
        <li>email: austin@jumanji.io (Preferably encrypted)</li>
        <br />
        <li>PGP Fingerprint: E367 81A0 9018 CAD4 24A5 E3A5 5572 CC1A A449 C6E6</li>
        <br />
        <li><a href='https://pgp.mit.edu/pks/lookup?op=get&search=0x5572CC1AA449C6E6' target='_blank'>PGP Public Key</a></li>
        <br />
            <h1>Austin on the web:</h1>
            <br />
            <p>This is my professional/personal site where you can see my resume, some longer-form tech writing about stuff I'm tinkering with, and (eventually) a fleshed out portfolio. Below, some links to my other sites.</p>
            <hr />
              <li><span>I maintain a social media presence <a href='https://mastodon.jumanji.co' target='_blank'>on my mastodon instance</a>, which anyone can join. My mastodon account is located <a href='https://mastodon.jumanji.co/@foggy' target='_blank'>here.</a></span></li>
              <br />
              <li><span>For my critical comic writing, check out <a href='https://fuckupsomecomics.com' target='_blank'>my comic blog</a>.</span></li>
              <br />
              <li><span>For an aggregation of this site, my other sites, and more spur of the moment posts, check out <a href='https://jumanji.io' target='_blank'>jumanji.io</a></span></li>
        <br />
            </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PostListing;
