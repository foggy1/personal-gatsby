---
title: "Styles, styles"
cover: "https://unsplash.it/1152/300/?random?SuperLong"
category: "site"
date: "06/17/2018"
tags:
    - web design
    - personal
---

I'm sort of in-between projects at the moment, with one major project I'm not suuuuper at liberty to talk about yet (it's my first rather major journalistic undertaking, which is exciting and also horrifying), but in general I've been trying to write more. I've been journaling with the help of [org-journal](https://github.com/bastibe/org-journal) since I spend most of my waking hours with an emacs editor open. I've also found that having embraced emacs, I am often trying its various keyboard shortcuts as a matter of pure muscle memory across literally every application on my computer.

Lovely.

Anyway, I've taken to making the blog the primary focus of my website rather than the portfolio which I haven't filled out yet. That's to encourage me to keep writing but also because... well, I haven't filled the portfolio out yet! That's not to say I'm not working on anything. As you might know, I'm a big fan of Gatsby.js (it powers this site, it's crazy fast, and you can do some fairly advance front-end stuff with minor config and zero webpack BS), and I also spotted a very new headless cms, Tipe.io.

For those who don't know, the appeal of a headless CMS, specifically with respect to websites like this one, or even simple web apps, is that you can structure your site around copy without having to tie the copy to the codebase. For instance, up top on my site it currently says "Coding and comics and comics and coding." If I wanted to change that to something less opaque (pfft), I would have to make a change to the html file with that text in it, re-commit it, and make a new build of my site. This might not sound super inconvenient, but imagine larger copy changes on larger sites with more people working on them and higher demand for staying up to date.

Headless CMS's like Tipe allow for you to hook your copy to remote resources with modern API's (tipe uses graphql in addition to a more traditional REST api). I quite like the idea of being able to edit in a CMS, although I have nothing against emacs. To be honest, I'd do all my editing in a remote CMS; but, I do like having full control over the content I write and where it lives. In the long-term, I'm not crazy about writing all my stuff and keeping it on any kind of server that I can't directly control these days.

In any case, I say all this because, as far as I know, I was the first person to automate a bit of the build process between Tipe and Gatsby, and you can [view the code here](https://github.com/foggy1/trying-tipe). It's not a full-on source plugin for Gatsby, but it's good enough. And, honestly, despite the fact that I think a source plugin could be designed in the vein of something as robust as the Wordpress source plugin for Gatsby, I don't know if it's a great idea for something like Tipe. If you look at the code, you can see the whole thing works as it revolves around a graphql query particular to my project. Although it could be standardized with respect to a user picking a specific folder with an accepted conventional structure, I think it would have to be a community decision whether or not "convention over configuration" is the best way to go. If the source plugin forces that on people, it just seems non-ideal. And having a script dumb json and use _that_ as a filesource doesn't strike me as dealbreaking, although the build process is lengthened by the need to manually format the markdown (in addition to the fact that I haven't actually thought at all about how images would be handled).

In other news, I'm extremely excited about [this twitter bot](https://github.com/BooDoo/artcoma), the tweets of which you can view [here](https://twitter.com/EtruscanCeramic). I'm not sure whether it's the intersection of my fairly newfound love of museums and coding, or if I just appreciate a good meme-bot at this point in my life, but one thing that feels great is seeing something cool like this out in the world and being able to look at the code and understand how it works. Two years ago this would have completely mystified me and now I spend parts of my spare time picking this stuff apart and loving it.

In addition to the thing I can't really talk about, I've got _another_ thing I can't really talk about cooking up too. Hopefully I can share soon. Until then, maybe expect more posts? I never know. I'm all over twitter these days but sometimes I think this is a healthier way to go. We shall see.
